pipeline {
    agent any
    environment {
        GIT_REPO = "https://gitlab.com/technerd/maven-project.git"
        GIT_BRANCH = "*/main"
        MAVEN_GOAL = 'clean deploy -X -s settings_ci.xml'
    }
    tools {
        maven 'maven3'
        jfrog 'jfrog-cli'
    }

    stages {
        stage('Git Checkout') {
            steps {
                script {
                    checkout([$class: 'GitSCM',
                        branches: [[name: env.GIT_BRANCH]],
                        userRemoteConfigs: [[url: env.GIT_REPO]],
                        extensions: [[$class: 'CleanBeforeCheckout'], 
                        [$class: 'CloneOption', noTags: true, shallow: false, timeout: 120]]
                    ])
                }
            }
        }
        
        stage('Maven Build') {
            steps {
                script {
                    def rtServer = Artifactory.server('artifactoryServer')
                    rtServer.credentialsId = 'artifactoryUser'

                    def rtMaven = Artifactory.newMavenBuild()
                    rtMaven.run pom: 'pom.xml', goals: env.MAVEN_GOAL

                    rtMaven.resolver releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot'
                    rtMaven.deployer releaseRepo: 'libs-release-local', snapshotRepo: 'libs-snapshot-local', server: rtServer

                    rtMaven.deployer.deployArtifacts = true
                }
            }
        }

        stage('Artifact Archive') {
            steps {
                step([$class: 'ArtifactArchiver', artifacts: 'target/*.jar', fingerprint: true])
            }
        }
        
        stage("Publish build info") {
            steps {
                jf "rt build-publish"
            }
        }
    }
}
